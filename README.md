# twenty-first-challenge-submission
## Jie Shen (Jason) Ong code challenge submission

### Additional features:
* Compact View
* Chart in Live Results
* Vote count badges
* Bootstrap & Custom CSS sheet in ./public/stylesheets/index.css
* Description
* Form Validation

### To Run:
1. install with `npm install`
2. run server with `node start`
3. navigate to `localhost://3000`

### Note：
I also removed the template strings and replaced them with templateUrls linked to separate files in ./public/templates.

The code was getting really messy and i needed a way to organize it. I figured it would be easier on your eyes too. I hope you don't mind.

### Challenges :
* I was pretty unfamiliar with version 1.6 of Angular since Angular went through a pretty significant change after Angular 2. My experiences with angular were mostly with the latter version. Due to this unfamiliarity, i might have violated some conventions unintentionally. I apologize in advance for that.

