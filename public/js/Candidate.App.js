const app = angular.module("Candidate.App", ['ngRoute','googlechart']);

app.component("itmRoot", {
  controller: class {
    constructor() {
      // initialize view to false so that users see normal view.
      this.compactView = false;
      // total votes casted;
      this.totalVotes = 0;
      // set to keep track of names
      this.candidateNames = new Set();
      this.candidates = [{ name: "Puppies", votes: 10 ,description : "A puppy is a juvenile dog. All healthy puppies grow quickly after birth. A puppy's coat color may change as the puppy grows older."},
                         { name: "Kittens", votes: 12 , description : " After being born, kittens are totally dependent on their mother for survival and they do not normally open their eyes until after seven to ten days." },
                         { name: "Gerbils", votes: 7  , description : "Gerbils are just .... Gerbils"}];
      // Add up all existing votes;
      this.candidates.forEach((candidate)=>{
        this.candidateNames.add(candidate.name);
        this.totalVotes+=candidate.votes;
      })
    }

    onVote(candidate) {
      //Update candidate's votes as well as total votes everytime vote button is triggered
      candidate.votes++;
      this.totalVotes++;
      console.log(`Vote for ${candidate.name}`);

    }

    // function to check for duplicate names
    checkDuplicateName(candidate){
      return this.candidateNames.has(candidate.name);
    }

    onAddCandidate(candidate) {
      candidate.votes = 0;
      // Check if candidate passed into function has duplicate name or empty length
      if(this.candidateNames.has(candidate.name) || candidate.name.length == 0){
        console.log("Duplicate or empty candidate name. Add Unsuccessful");
        return; 
      }else{
        // if requirments are met, add candidates to candidate array.
        this.candidates.push({
          name : candidate.name,
          description : candidate.description,
          votes : 0
        });
        // keep track of existing candidate names
        this.candidateNames.add(candidate.name);
        console.log(`Added candidate ${candidate.name}`);
      }
    }

    onRemoveCandidate(candidate) {
      // remove candidate votes from total votes as well as candidate name from set;
      this.totalVotes -= candidate.votes;
      this.candidateNames.delete(candidate.name);
      // remove candidates from array
      this.candidates = this.candidates.filter( (c)=> candidate.name !== c.name);
      console.log(`Removed candidate ${candidate.name}`);
    }
    //toggles view, shows and hides compact view.
    toggleCompact(){
      this.compact = !this.compact;
    }
  },
      templateUrl: "../templates/itm-root.html"
});

app.component("itmManagement", {
    bindings: {
        candidates: "<",
        onAdd: "&",
        onRemove: "&",
        onCheck:"&"
    },
    controller: class {
        constructor() {
            // initialize new candidate to have empty name and description
            this.newCandidate = {
                name: "",
                description : ""
            };
        }

        submitCandidate(candidate) {
            this.onAdd({ $candidate: candidate });
        }

        removeCandidate(candidate) {
            this.onRemove({ $candidate: candidate });
        }
        nameCheck(candidate) {
            return this.onCheck({ $candidate: candidate });
        }
    },
    templateUrl: "../templates/itm-management.html"
});

app.component("itmVote", {
    bindings: {
        candidates: "<",
        onVote: "&"
    },
    controller: class {},
    templateUrl: "../templates/itm-vote.html"
});

app.component("itmResults", {
    bindings: {
        candidates: "<",
        vc: "<" // vote counts
    },
    controller: class {},
    templateUrl: "../templates/itm-results.html"
});

// A component that displays a pie chart for the live results using the google charts API
app.component("resultschart", {
  bindings: {
    data: "<"
  },
  controller: class {
    constructor($scope) {

      // initialize all parameters.
      $scope.myChartObject = {};
      $scope.myChartObject.type = "PieChart";
      $scope.myChartObject.data = {"cols": [
        {id: "name", label: "Candidate Name", type: "string"},
        {id: "votes", label: "Votes", type: "number"}
      ],
      "rows": []
    };
    
    // updates chart;
    this.initializeData = ()=>{
      var dataArray = new Array();
      this.data.forEach((candidate)=>{
        dataArray.push({
          c:[
            {v : candidate.name},
            {v : candidate.votes}
          ]
        })
      });
      $scope.myChartObject.data.rows = dataArray;
    };
    $scope.initializeData = this.initializeData;
  }
},
    template: `
        <div ngIf = "$ctrl.data!==undefined">
          {{initializeData()}}
          <div class = "results-chart" google-chart chart="myChartObject"></div>
        </div>
    `
});
